var express = require('express');
var graphqlHTTP = require('express-graphql');
var { buildSchema } = require('graphql');
var menegaers = require('./menagers');

let fakeDatabase = {};
class Message {
   constructor(id, {content, author}) {
      this.id = id;
      this.content = content;
      this.author = author;
   }
}

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
   type Name {
      first_name: String
      last_name: String
   }
   
   type Manager {
      id: ID
      name: Name
      last: Boolean
      type: String!
   }
   
   type Message {
      id: ID!
      content: String
      author: String
   }
   
   input MessageInput {
      content: String
      author: String
   }
   
   type Mutation {
      createMessage(input: MessageInput): Message
      updateMessage(id: ID!, input: MessageInput): Message
   }
   
   type Query {
      getMessage(id: String): Message
      getmanager: [Manager]
      calcName(first: String): String
      calcFullName(last: String, first: String): String
  }
`);

// The root provides a resolver function for each API endpoint
var root = {
   getMessage: ({id}) => {
      if (!fakeDatabase[id]) {
         throw new Error('no message exists with id ' + id);
      } else {
         return new Message(id, fakeDatabase[id]);
      }
   },
   createMessage: ({input}) => {
      console.log(input)
      
      // Create a random id for our "database".
      var id = require('crypto').randomBytes(10).toString('hex');

      fakeDatabase[id] = input;
      return new Message(id, input);
   },
   updateMessage: ({id, input}) => {
      if (!fakeDatabase[id]) {
         throw new Error('no message exists with id ' + id);
      }
      // This replaces all old data, but some apps might want partial update.
      fakeDatabase[id] = input;
      return new Message(id, input);
   },

   
   getmanager: () => {
      return menegaers
   },
   calcName: ({first}) => {
      return `${first}`
   },
   calcFullName: ({last, first}) => {
      return `${last} ${first}`
   }
};

//    /*query*/
// query ($last:String, $first:String){
//    calcName(first: $first)
//    calcFullName(last: $last, first: $first)
// }
//
//    /*variables*/
// {
//    "last": "Nikolay",
//    "first": "Polupanov"
// }

var app = express();
app.use('/graphql', graphqlHTTP({
   schema: schema,
   rootValue: root,
   graphiql: true,
}));
app.listen(5000);
console.log('Running a GraphQL API server at http://localhost:5000/graphql');