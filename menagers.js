module.exports = [
   {
      id: 1,
      name: {
         first_name: 'Polupanov',
         last_name: 'Nikolay',
      },
      last: true
   },
   {
      id: 2,
      name: {
         first_name: 'Volkanovsky',
         last_name: 'Alexandr',
      },
      last: false
   },
   {
      id: 3,
      name: {
         first_name: 'Negruca',
         last_name: 'Konstantin',
      },
      last: true
   },
]